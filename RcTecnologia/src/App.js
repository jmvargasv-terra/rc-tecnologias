import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { store, persistor } from './store'
import { PersistGate } from 'redux-persist/integration/react'
import AppNavigationWithState from './navigation/app-navigation-with-state'
import { StyleProvider } from 'native-base'
import getTheme from '../native-base-theme/components'
import {
  SafeAreaView 
} from 'react-native'

class App extends Component { 
  render() {
    console.disableYellowBox = true 
    return (  
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaView style={{ flex: 1 }}>
            <StyleProvider style={getTheme()}>
              <AppNavigationWithState />
            </StyleProvider>
          </SafeAreaView>
        </PersistGate>
      </Provider>
    )
  }
}

export default App

import AppNavigator from '../navigation/app-navigator'
import {
    createNavigationReducer
} from 'react-navigation-redux-helpers'

const navigationReducer = createNavigationReducer(AppNavigator)

export default navigationReducer 
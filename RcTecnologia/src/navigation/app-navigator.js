import Home from '../screens/home/home'
import { 
    createStackNavigator,
    createAppContainer,
    createSwitchNavigator, 
    createDrawerNavigator 
 } from 'react-navigation' 


const SwitchNavigator = createSwitchNavigator(
    {
        Home
    },
    {
      initialRouteName: 'Home'
    }
  )

const App = createAppContainer(SwitchNavigator)


export default App
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import AppNavigator from './app-navigator'
import {
  reduxifyNavigator,
} from 'react-navigation-redux-helpers'
import {
  BackHandler
} from 'react-native'
 
const ReduxifyApp = reduxifyNavigator(AppNavigator, 'root')

class AppNavigatorWithState extends ReduxifyApp {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
  }
  onBackPress = () => {
    this.props.dispatch(NavigationActions.back({
      key: null
    })
    )
    return true
  }
}

function mapStateToProps(state) {
  return {
    state: state.navigation 
  }
}

export default connect(mapStateToProps)(AppNavigatorWithState)
import React, {Component} from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'

class Home extends Component{
    constructor(props){
        super(props)
    }

render(){
    return(
            <View style={Style.container}>
                <Text
                    style={Style.centrar}
                >hola mundo</Text>
            </View>
        )
    }
}

const Style = StyleSheet.create({
    centrar:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    container:{
        flex: 1
    }
})

export default Home
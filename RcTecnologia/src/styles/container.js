import { StyleSheet, Dimensions } from 'react-native'
const { width } = Dimensions.get('window')
import color from './color'

export default StyleSheet.create({
    fullScream:{
        flex: 1,
        backgroundColor: color.hex.white,
        width: width
    }
}) 
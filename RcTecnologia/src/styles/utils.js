import { Dimensions } from 'react-native'

const { height, width } = Dimensions.get('window')

const getAdjustedFontSize = (size) => {
  return parseInt(size) * width / 400
}

const calculateWidth = (percentage) => {
  return (width * percentage)
}

const calculateHeight = (percentage) => {
  return (height * percentage)
}

export {
  getAdjustedFontSize,
  calculateWidth,
  calculateHeight
} 
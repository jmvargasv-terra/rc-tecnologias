import { StyleSheet } from 'react-native'
import { calculateHeight, calculateWidth } from './utils'

const marginTop = StyleSheet.create({
    1.5:{
        marginTop: calculateHeight(0.002)
    },
    6.4:{
        marginTop: calculateHeight(0.008)
    },
    19:{
        marginTop: calculateHeight(0.0250)
    },
    8.1:{
        marginTop:calculateHeight(0.0107)
    },
    32:{
        marginTop: calculateHeight(0.0421)
    },
    69.5:{
        marginTop: calculateHeight(0.0914)
    },
    29:{
        marginTop: calculateHeight(0.0382)
    },
    42:{
        marginTop: calculateHeight(0.0553)
    },
    98.4:{
        marginTop:calculateHeight(0.1)
    }
})

const marginLeft = StyleSheet.create({
    8:{
        marginLeft: calculateWidth(0.0213)
    },
    5:{
        marginLeft: calculateWidth(0.015)
    },
    4:{
        marginLeft: calculateWidth(0.01)
    },
    11.7:{
        marginLeft: calculateWidth(0.0305)
    },
    18.2:{
        marginLeft: calculateWidth(0.0485)
    },
    20:{
        marginLeft: calculateWidth(0.12)
    }
})

const marginRight = StyleSheet.create({
    8:{
        marginRight: calculateWidth(0.0213)
    },
    11.7:{
        marginRight: calculateWidth(0.0305)
    },
    20:{
        marginRight: calculateWidth(0.12)
    },
    12:{
        marginRight: calculateWidth(0.03)
    }
})

const marginBottom = StyleSheet.create({
    85:{
        marginBottom: calculateHeight(0.1118)
    },
    60:{
        marginBottom: calculateHeight(0.0618)
    },
    45:{
        marginBottom: calculateHeight(0.0458)
    },
    30:{
        marginBottom: calculateHeight(0.0358)
    },
    11:{
        marginBottom: calculateHeight(0.018)
    },
    9:{
        marginBottom: calculateHeight(0.01)
    }
    ,
    5:{
        marginBottom: calculateHeight(0.007)
    }
})

const horizontal =StyleSheet.create({
    5:{
        marginHorizontal: calculateWidth(0.0133) 
    },
    7:{
        marginHorizontal: calculateWidth(0.0187)
    },
    20:{
        marginHorizontal: calculateWidth(0.0533)
    },
    31:{
        marginHorizontal: calculateWidth(0.0824)
    },
    50:{
        marginHorizontal: calculateWidth(0.1333)
    },
    70:{
        marginHorizontal: calculateWidth(0.18)
    },
    10:{
        marginHorizontal: calculateWidth(0.0275)
    },
    12:{
        marginHorizontal: calculateWidth(0.037)
    }
})

    const vertical = StyleSheet.create({
        1.5:{
            marginTop: calculateHeight(0.002)
        },
        6.4:{
            marginTop: calculateHeight(0.008)
        },
        19:{
            marginTop: calculateHeight(0.0250)
        },
        8.1:{
            marginTop:calculateHeight(0.0107)
        },
        32:{
            marginTop: calculateHeight(0.0421)
        },
        69.5:{
            marginTop: calculateHeight(0.0914)
        },
        29:{
            marginTop: calculateHeight(0.0382)
        },
        42:{
            marginTop: calculateHeight(0.0553)
        },
        98.4:{
            marginTop:calculateHeight(0.1)
        }
    })




export default {
    marginTop,
    marginLeft,
    marginRight,
    marginBottom,
    horizontal,
    vertical
} 
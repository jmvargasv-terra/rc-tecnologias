import { StyleSheet } from 'react-native'

const Position = StyleSheet.create({
    justifyLeft:{

        justifyContent: 'flex-start'
    },
    justifyRight:{

        justifyContent: 'flex-end' 
    },
    justifyCenter:{

        justifyContent: 'center'
    },
    alignLeft:{

        alignItems: 'flex-start'
    },
    alignRight:{

        alignItems: 'flex-end'
    }, 
    alignCenter: {

        alignItems: 'center'
        
      },
    row:{
        flexDirection: 'row'
    }

})

export default Position